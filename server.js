const express = require('express')

const {graphqlHTTP} = require('express-graphql')
const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLString

} = require('graphql')
const app = express()

const schema = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'hello world',
        fields: () => ({
            message:{
                 type: GraphQLString,
                 resolve: () => 'hello world'
                }
        })
    })
})

app.use('/graphql', graphqlHTTP({
    graphiql:true
    
}))
app.listen(5000., () => console.log('sever Runing'))